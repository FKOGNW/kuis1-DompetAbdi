package edu.upi.cs.yudiwbs.dompetabdi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;

import java.util.LinkedList;
import java.util.List;

public class OpenHelper extends SQLiteOpenHelper {

    private static final int  DATABASE_VERSION= 1;
    public static final String DATABASE_NAME= "dbTransaksi.db";
    public final String TABLE_NAME= "Transaksi";
    public final String COLUMN_ID= "ID";
    public final String COLUMN_JENIS= "Jenis";
    public final String COLUMN_JUMLAH= "Jumlah";
    public final String COLUMN_CATATAN= "Catatan";

    // LENGKAPI sesuaikan dengan class Transaksi.java

    public OpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_JENIS+ " TEXT , " +
                COLUMN_JUMLAH + " TEXT,"+
                COLUMN_CATATAN+ "TEXT )");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS Transaksi");
        this.onCreate(sqLiteDatabase);
    }

    public void saveNewTransaksi(Transaksi transaksi){
        SQLiteDatabase db= this.getWritableDatabase();
        ContentValues values= new ContentValues();
        values.put(COLUMN_JENIS, transaksi.getJenisTransaksi());
        values.put(COLUMN_JUMLAH, transaksi.getJumlah());
        values.put(COLUMN_CATATAN, transaksi.getCatatan());
//        values.put("IMAGE", "akka");
        Log.i("FK", "apa"+transaksi.getCatatan());
//        values.put("IMAGE", menu.getImage());

//        insert
        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public List<Transaksi> transaksiList(String filter){
        String query;
        if(filter.equals("")){
//            regular query
            query= "SELECT * FROM "+TABLE_NAME;
        }else{
//            filter result by filter option provided
            query= "SELECT * FROM "+TABLE_NAME+" ORDER BY"+ filter;
        }

        List<Transaksi> transaksiLinkedList= new LinkedList<>();
        SQLiteDatabase db= this.getWritableDatabase();
        Cursor cursor= db.rawQuery(query, null);
        Transaksi transaksi;

        if(cursor.moveToFirst()){
            do{
                transaksi= new Transaksi();

                transaksi.setId(cursor.getLong(cursor.getColumnIndex(COLUMN_ID)));
                transaksi.setJenisTransaksi(cursor.getString(cursor.getColumnIndex(COLUMN_JENIS)));
                transaksi.setJumlah(cursor.getString(cursor.getColumnIndex(COLUMN_JUMLAH)));
                transaksi.setJumlah(cursor.getString(cursor.getColumnIndex(COLUMN_CATATAN)));
//                menu.setImage(cursor.getString(cursor.getColumnIndex("IMAGE")));

                transaksiLinkedList.add(transaksi);
            }while (cursor.moveToNext());
        }
        return transaksiLinkedList;
    }


    /*if only one record*/
    public Transaksi getTransaksi(Long id){
        SQLiteDatabase db= this.getWritableDatabase();
        String query= "SELECT * FROM "+TABLE_NAME+" WHERE ID="+id;
        Cursor cursor= db.rawQuery(query, null);

        Transaksi receivedTransaksi= new Transaksi();
        if(cursor.getCount()> 0){
            cursor.moveToFirst();

            receivedTransaksi.setJenisTransaksi(cursor.getString(cursor.getColumnIndex(COLUMN_JENIS)));
            receivedTransaksi.setJumlah(cursor.getString(cursor.getColumnIndex(COLUMN_JUMLAH)));
            receivedTransaksi.setCatatan(cursor.getString(cursor.getColumnIndex(COLUMN_CATATAN)));
//            receivedMenu.setImage(cursor.getString(cursor.getColumnIndex("IMAGE")));
        }

        return receivedTransaksi;
    }
}
