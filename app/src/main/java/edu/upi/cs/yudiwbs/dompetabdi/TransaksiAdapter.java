package edu.upi.cs.yudiwbs.dompetabdi;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class TransaksiAdapter extends RecyclerView.Adapter<TransaksiAdapter.ViewHolder> {
    private List<Transaksi> mTransaksiList;
    private Context mContext;
    private RecyclerView mRecyclerV;

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvJenisTransaksi;
        public TextView tvJumlah;
        public TextView tvCatatan;

        public View layout;
        public ViewHolder(View itemView) {
            super(itemView);
            layout= itemView;

            tvJenisTransaksi= itemView.findViewById(R.id.tvJenisTransaksi);
            tvJumlah= itemView.findViewById(R.id.tvJumlah);
            tvCatatan= itemView.findViewById(R.id.tvCatatan);
        }
    }

    public void add(int position, Transaksi transaksi){
        mTransaksiList.add(position, transaksi);
        notifyItemInserted(position);
    }

    public TransaksiAdapter(List<Transaksi> myDataset, Context context, RecyclerView recyclerView) {
        mTransaksiList = myDataset;
        mContext = context;
        mRecyclerV = recyclerView;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        /*create new view*/
        LayoutInflater infalter= LayoutInflater.from(parent.getContext());

        View v= infalter.inflate(R.layout.transaksi_row, parent, false);

        ViewHolder vh= new ViewHolder(v);
        return vh;
//        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        final Transaksi transaksi= mTransaksiList.get(position);
        holder.tvJenisTransaksi.setText("Jenis : "+ transaksi.getJenisTransaksi());
        holder.tvJumlah.setText("Jumlah : "+transaksi.getJumlah());
        holder.tvCatatan.setText("Catatan : "+transaksi.getCatatan());

    }

    @Override
    public int getItemCount() {
        return mTransaksiList.size();
    }
}
