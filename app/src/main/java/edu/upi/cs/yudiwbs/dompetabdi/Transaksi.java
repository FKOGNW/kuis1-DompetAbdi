package edu.upi.cs.yudiwbs.dompetabdi;

public class Transaksi {

    //LENGKAPI
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    String jenisTransaksi; //ada dua: top-up dan transfer (bayar)
    String jumlah; //jumlah yang ditransaksikan

    //untuk topup, catatan berisi cara  top-up (misal "via trasnfer")
    // sedangkan untuk tranfer catatan berisi id tujuan (misal "transfer ke @budi")
    String catatan;

    public Transaksi(String jenisTransaksi, String jumlah, String catatan) {
        this.jenisTransaksi = jenisTransaksi;
        this.jumlah = jumlah;
        this.catatan = catatan;
    }

    public Transaksi(){}

    public String getJenisTransaksi() {
        return jenisTransaksi;
    }

    public void setJenisTransaksi(String jenisTransaksi) {
        this.jenisTransaksi = jenisTransaksi;
    }

    public String getJumlah() {
        return jumlah;
    }

    public void setJumlah(String jumlah) {
        this.jumlah = jumlah;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan;
    }
}
