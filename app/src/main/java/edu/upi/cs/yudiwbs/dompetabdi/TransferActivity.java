package edu.upi.cs.yudiwbs.dompetabdi;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TransferActivity extends AppCompatActivity {

    private String jenisTransaksi;
    private EditText etJumlah;
    private EditText etCatatan;
    private Button btTransfer;

    private OpenHelper dbHelper;

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        jenisTransaksi= "Transfer";
        etJumlah= findViewById(R.id.etJumlah);
        etCatatan= findViewById(R.id.etCatatan);

        btTransfer= findViewById(R.id.btTransfer);

        btTransfer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.i("Fk","masuk button");
                addTransaksi();
            }
        });
    }

    private void addTransaksi() {
        String jenis= jenisTransaksi;
        String jumlah= etJumlah.getText().toString().trim();
        Log.i("Fk",jumlah);
        String catatan= etCatatan.getText().toString().trim();

//        dbHelper= new OpenHelper(this);

        dbHelper= new OpenHelper(this);
        if(jumlah.isEmpty() || catatan.isEmpty()){
            Toast.makeText(this, "Tolong lengkapi transaksi terlebih dahulu", Toast.LENGTH_SHORT).show();
        }

        /*create new Transaksi*/
        Transaksi transaksi= new Transaksi(jenis, jumlah, catatan);
        dbHelper.saveNewTransaksi(transaksi);

        goBackHome();
    }

    private void goBackHome() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }
}
