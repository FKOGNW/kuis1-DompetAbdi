package edu.upi.cs.yudiwbs.dompetabdi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    /*inisiasi recyclerview*/
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private OpenHelper dbHelper;
    private TransaksiAdapter adapter;
    private  String filter= "";

    Toolbar myToolbarj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);


        /*initialize recyclerview*/
        mRecyclerView= findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);

        /*use liner layout manager*/
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        /*populate recyclerview*/
        populateRecyclerView(filter);
    }

    private void populateRecyclerView(String filter) {
        dbHelper= new OpenHelper(this);
        adapter= new TransaksiAdapter(dbHelper.transaksiList(filter), this, mRecyclerView);
        mRecyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.mTransfer:
                Intent intentTransfer = new Intent(this, TransferActivity.class);
                startActivity(intentTransfer);
                return true;
            case R.id.mTopUp:
                Intent intentTopUp = new Intent(this, TopUpActivity.class);
                startActivity(intentTopUp);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
